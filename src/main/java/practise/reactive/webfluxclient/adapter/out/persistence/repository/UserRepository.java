package practise.reactive.webfluxclient.adapter.out.persistence.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;

@Repository
public interface UserRepository extends ReactiveCrudRepository<Users, Long> {
}
