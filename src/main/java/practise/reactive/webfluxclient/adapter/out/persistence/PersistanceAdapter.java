package practise.reactive.webfluxclient.adapter.out.persistence;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;
import practise.reactive.webfluxclient.adapter.out.persistence.repository.UserRepository;
import practise.reactive.webfluxclient.application.port.out.UserServicePort;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class PersistanceAdapter implements UserServicePort {
    @Autowired
    UserRepository repository;

    @Override
    public Mono<Users> getUserById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Mono<Void> addUser(Users user) {
        repository.save(user);
        return null;
    }

    @Override
    public Flux<Users> getUsers() {
        return repository.findAll();
    }

    @Override
    public Mono<Users> updateUser(Users user) {
        return repository.save(user);
    }

    @Override
    public Mono<Void> deleteUser(Long id) {
        repository.deleteById(id);
        return null;
    }
}
