package practise.reactive.webfluxclient.adapter.in;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;
import practise.reactive.webfluxclient.application.port.in.UsersUseCase;
import practise.reactive.webfluxclient.application.service.UserService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UsersUseCase usersUseCase;

    @GetMapping(value = "/users", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Flux<Users> findAllUsers() {
        return usersUseCase.findAllUsers();
    }

    @GetMapping("/user/{id}")
    public Mono<Users> findUserById(@PathVariable Long id) {
        return usersUseCase.findUserById(id);
    }

    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveUser(@RequestBody Users users) {
        usersUseCase.saveUser(users);
    }


    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Users> updateUser(@RequestBody Users user) {
        return usersUseCase.updateUser(user);
    }

    @DeleteMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteUser(@PathVariable Long id) {
        return usersUseCase.deleteUser(id);
    }

}