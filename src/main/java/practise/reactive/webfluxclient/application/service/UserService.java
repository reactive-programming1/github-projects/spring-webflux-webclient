package practise.reactive.webfluxclient.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;
import practise.reactive.webfluxclient.application.port.in.UsersUseCase;
import practise.reactive.webfluxclient.application.port.out.UserServicePort;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class UserService implements UsersUseCase {
    @Autowired
    UserServicePort servicePort;


    @Override
    public Flux<Users> findAllUsers() {
        return servicePort.getUsers()
                .delayElements(
                        Duration.ofSeconds(1)
                );
    }

    @Override
    public Mono<Users> findUserById(Long id) {
        return servicePort.getUserById(id);

    }

    @Override
    public void saveUser(Users users) {
        servicePort.addUser(users).subscribe();
    }

    @Override
    public Mono<Users> updateUser(Users user) {
        return servicePort.getUserById(user.getId())
                .switchIfEmpty(Mono.error(new Exception("User Not Found")))
                .map(olderUser -> {
                    if (user.getSurname() != null) olderUser.setSurname(user.getSurname());
                    if (user.getUsername() != null) olderUser.setUsername(user.getUsername());
                    if (user.getName() != null) olderUser.setName(user.getName());
                    if (user.getEmail() != null) olderUser.setEmail(user.getEmail());
                    return olderUser;
                })
                .flatMap((Users) -> servicePort.updateUser(user));
    }

    @Override
    public Mono<Void> deleteUser(Long id) {
        return servicePort.deleteUser(id)
                .switchIfEmpty(Mono.error(new Exception("User Not found")));
    }

}
