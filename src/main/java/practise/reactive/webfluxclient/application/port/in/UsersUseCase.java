package practise.reactive.webfluxclient.application.port.in;

import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UsersUseCase {
    Flux<Users> findAllUsers();

    Mono<Users> findUserById(Long id);

    void saveUser(Users users);

    Mono<Users> updateUser(Users user);

    Mono<Void> deleteUser(Long id);
}