package practise.reactive.webfluxclient.application.port.out;

import practise.reactive.webfluxclient.adapter.out.persistence.entity.Users;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserServicePort {
    Mono<Void> addUser(Users user);

    Mono<Users> getUserById(Long id);

    Flux<Users> getUsers();

    Mono<Users> updateUser(Users user);

    Mono<Void> deleteUser(Long id);
}
